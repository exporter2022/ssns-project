package com.example.bluetoothtest;

import android.util.Log;
import android.widget.Toast;

public class Decider {
    public static final String TAG = "HomeAutomation";
    final double LightCriticalPoint = 200;
    final int StepCriticalPoint = 5;
    final double ProximityCriticalPoint = 1;
    final float RSSICriticalPoint = 1;
    boolean isAutomated = false;
    boolean isLightOn = false;
    int oldStepCount = 0;
    int oldRSSI = 0;
    int currentRSSI  = 0;
    double distance = 0;
    double distanceCritical = 0.38;
    ISensorMeasurement measurement;
    ILightController lightCtl;
    public Decider(ISensorMeasurement measurement) {
        // decider
        this.measurement = measurement;
    }

    public void setController(ILightController lightCtl) {
        this.lightCtl = lightCtl;
    }

    public void decide() {
        // check if it is automated
        if(isAutomated && this.lightCtl != null) {
            // get rssi value
            if(StoragePreference.get() != 0) {
                currentRSSI = StoragePreference.get();
                Log.i(TAG,  "Current RSSI : "+ currentRSSI);
            }

            distance = Math.pow(10, ((-61.5 - currentRSSI)/ (10 * 2.75))); //10^(A0-RSSI)/10*n

            // check if light is high in below critical point &
            // light is not on &
            // stepCount diff is significant
            int currentStep=measurement.getStep();
            float currentLight = measurement.getLight();
            if(!isLightOn &&
                    (currentStep - oldStepCount > StepCriticalPoint) &&
                    currentLight < LightCriticalPoint &&
                    distance <= distanceCritical
            ) {
                isLightOn = true;
                lightCtl.turnOn();
                oldStepCount = measurement.getStep();
            } else {
                // is light on
                // is there significant step change
                // is rssi value decreasing
                currentStep = measurement.getStep();
                if(isLightOn && ( currentStep - oldStepCount > StepCriticalPoint) && distance > distanceCritical) {
                    isLightOn = false;
                    lightCtl.turnOff();
                    oldStepCount = measurement.getStep();
                }
            }
        }
    }
}
