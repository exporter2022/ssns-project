package com.example.bluetoothtest;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;


public class StoragePreference extends Application {
    static int rssi;
    static String device;

    public static void set(int value){
        StoragePreference.rssi=value;
    }

    public static int get(){
        return StoragePreference.rssi;
    }
}
