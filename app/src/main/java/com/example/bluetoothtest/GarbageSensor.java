package com.example.bluetoothtest;

public class GarbageSensor {
    // static variable single_instance of type Singleton
    private static GarbageSensor single_instance = null;

    // variable of type String
    public String s;
    private int rssi;
    private String address;
    // private constructor restricted to this class itself
    private GarbageSensor()
    {
        s = "Hello I am a string part of Singleton class";
    }

    public void setValue(String addr,  int value) {
        rssi = value;
        address = addr;
    }

    public int getValue() {
        return rssi;
    }
    public String getAddress() { return address; }
    // static method to create instance of Singleton class
    public static GarbageSensor getInstance()
    {
        if (single_instance == null)
            single_instance = new GarbageSensor();

        return single_instance;
    }
}
