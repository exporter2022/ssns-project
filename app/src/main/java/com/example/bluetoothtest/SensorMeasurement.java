package com.example.bluetoothtest;

public class SensorMeasurement implements ISensorMeasurement {
    float light = 0;
    float proximity = 0;
    float accelerometer[] = {0, 0, 0};
    int step = 0;
    float rssi = 0;
    public void setLight(float newValue) {
        light = newValue;
    }

    public void setProximity(float newValue) {
        proximity = newValue;
    }

    public void setAccelerometer(float[] newValue) {
        accelerometer = newValue;
    }

    public void setStep(int newValue) {
        step = newValue;
    }

    public void setRSSI(float newVlaue) { rssi = newVlaue; }

    public float getLight() {
        return light;
    }

    public float getProximity() {
        return proximity;
    }

    public float[] getAccelerometer() {
        return accelerometer;
    }

    public int getStep() { return step; }

    public float getRSSI() { return rssi; }
}
