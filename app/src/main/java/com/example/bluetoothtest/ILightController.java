package com.example.bluetoothtest;

public interface ILightController {
    public void turnOn();
    public void turnOff();
}
