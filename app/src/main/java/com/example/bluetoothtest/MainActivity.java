/**
 * MainActivity.java -  class where bluetooth functionality is implemented..
 * @author  Sahithi Rukanana
 *
 */
package com.example.bluetoothtest;

import android.Manifest;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.util.Log;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Set;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.Date;
import com.example.bluetoothtest.UartService;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
//import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.view.ViewGroup.LayoutParams;
public class MainActivity extends Activity  implements SensorEventListener {
 BluetoothGatt gatt;
    private int rssi = 0;
    // from sensor reading
    private SensorManager mSensorManager;
    private ISensorMeasurement measurement;
    private ILightController lightController;
    private Decider decider;
    private Sensor mLight;
    private Sensor mProx;
    private Sensor mAcc;
    private Sensor mStepSensor;
    private boolean isSensorPresent = false;
    private float currentLux = 0;
    private float currentProximity = 2;
    private float x = 0;
    private float y = 0;
    private float z = 0;
    private GarbageSensor gbg = GarbageSensor.getInstance();
    private int currentStep = 0;
    private float maxLux;
    private final double MAXALPHA = 1;
    private double alpha;
    private TextView lightView;
    private TextView stepCountView;
    private TextView rssiView;
    private TextView accX;
    private TextView accY;
    private TextView accZ;
    private TextView output;
    float[] accAgg = {0, 0, 0};
    //end of sensor reading
    public static final String TAG = "HomeAutomation";
    private static final int REQUEST_SELECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int UART_PROFILE_READY = 10;
    private static final int UART_PROFILE_CONNECTED = 20;
    private static final int UART_PROFILE_DISCONNECTED = 21;
    private final int PHYISCAL_ACTIVITY=1;
    //private static final int STATE_OFF = 10;

    private int mState = UART_PROFILE_DISCONNECTED;
    private UartService mService = null;
    private ArrayAdapter<String> listAdapter;

    Button btnCon,btnOff,btnOn, btnMode;

    private BluetoothDevice mDevice = null;
    private BluetoothAdapter BAdapter =null;
    private Set<BluetoothDevice>pairedDevices;
    private Button btnSend;
    private EditText edtMessage;
    private Intent gintent = null;
    private ListView lview;
    private ListView messageListView;
    LayoutParams layoutparams;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // from sensor data
        stepCountView = (TextView) findViewById(R.id.stepCountValue);
        lightView = (TextView) findViewById(R.id.lightValue);
        rssiView = (TextView) findViewById(R.id.rssiValue);
//        accX = (TextView) findViewById(R.id.accValueX);
//        accY = (TextView) findViewById(R.id.accValueY);
//        accZ = (TextView) findViewById(R.id.accValueZ);
//        output = (TextView) findViewById(R.id.output);
        this.mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        this.mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        this.mAcc = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        this.mProx = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        if(mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)
                != null)
        {
            mStepSensor =
                    mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
            isSensorPresent = true;
        }
        else
        {
            isSensorPresent = false;
        }

        Toast.makeText(getApplicationContext(), "Got value" + isSensorPresent, Toast.LENGTH_LONG).show();
        //maxLux = this.mLight.getMaximumRange();
        mSensorManager.registerListener((SensorEventListener) this, mLight, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener((SensorEventListener) this, mAcc, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener((SensorEventListener) this, mProx, SensorManager.SENSOR_DELAY_NORMAL);
        measurement = new SensorMeasurement();
        decider = new Decider(measurement);
        // end of sensor data
        btnCon = (Button) findViewById(R.id.btn_select);
        btnOn = (Button) findViewById(R.id.offbutton);
        btnOff = (Button)findViewById(R.id.onbutton);
        btnMode = (Button)findViewById(R.id.autoBtn);
       // button3 = (Button)findViewById(R.id.button3);
       // button4 = (Button)findViewById(R.id.button4);
        messageListView = (ListView) findViewById(R.id.listMessage);
        listAdapter = new ArrayAdapter<String>(this, R.layout.message_detail);
        messageListView.setAdapter(listAdapter);
        messageListView.setDivider(null);
        BAdapter = BluetoothAdapter.getDefaultAdapter();
        //btnSend=(Button) findViewById(R.id.sendButton);
        //edtMessage = (EditText) findViewById(R.id.sendText);
        layoutparams = messageListView.getLayoutParams();
        layoutparams.height = 40;
        messageListView.setLayoutParams(layoutparams);
        if (BAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

//        ((TextView) findViewById(R.id.stepCountValue)).setText(currentStep);
        stepCountView = (TextView) findViewById(R.id.stepCountValue);
        //stepCountView.setText(""+currentStep);
        service_init();
        /*else {
            //lview = (ListView) findViewById(R.id.listView);
        }*/
    }

    // from sensor data
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        if (sensor.getType() == Sensor.TYPE_LIGHT) {
        }
    }
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
            currentLux = (float) event.values[0];
            NumberFormat formatter = new DecimalFormat("#0");
            lightView.setText(formatter.format(currentLux));
            //lightValue.setText(formatter.format(currentLux));
        }

        if(event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
            currentProximity = (float) event.values[0];
            NumberFormat formatter1 = new DecimalFormat("#0");
            //proximityValue.setText(formatter1.format(currentProximity));
        }

        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            accAgg[0] = (float) event.values[0];
            accAgg[1] = (float) event.values[1];
            accAgg[2] = (float) event.values[2];
            NumberFormat formatter2 = new DecimalFormat("#0");
//            accX.setText(formatter2.format(accAgg[0]));
//            accY.setText(formatter2.format(accAgg[1]));
//            accZ.setText(formatter2.format(accAgg[2]));
        }

        if(event.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
            //Toast.makeText(getApplicationContext(), "Got value" + String.valueOf(event.values[0]), Toast.LENGTH_LONG).show();
            stepCountView.setText(String.valueOf(event.values[0]));
            currentStep = (int) event.values[0];
        }

        if(event.sensor.getType() == Sensor.TYPE_LIGHT || event.sensor.getType() == Sensor.TYPE_PROXIMITY || event.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
            // set measurement and call decide
            measurement.setRSSI((float)gbg.getValue());
           // rssiView.setText(String.valueOf(gbg.getValue()));
            measurement.setLight(currentLux);
            measurement.setStep(currentStep);
            measurement.setProximity(currentProximity);
            if(mService != null && mDevice != null) {
                lightController = new LightController(mService, mDevice);
                decider.setController(lightController);
                decider.decide();
            }
        }

        //Toast.makeText(getApplicationContext(), "In sensor change method" + String.valueOf(ins.getAddress()), Toast.LENGTH_LONG).show();
        Toast.makeText(getApplicationContext(), "In sensor change method rssi value: " + StoragePreference.get(), Toast.LENGTH_LONG).show();
    }
    // end sensor data

    public void on(View v){
        if (!BAdapter.isEnabled()) {
            Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOn, 0);
            Toast.makeText(getApplicationContext(), "Turned ON",Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "Already ON", Toast.LENGTH_LONG).show();
        }
    }

    public void off(View v){
        BAdapter.disable();
        Toast.makeText(getApplicationContext(), "Turned OFF" ,Toast.LENGTH_LONG).show();
    }

    public void list(View v){
        pairedDevices = BAdapter.getBondedDevices();
        ArrayList plist = new ArrayList();

        for(BluetoothDevice bt : pairedDevices) plist.add(bt.getName());
        Toast.makeText(getApplicationContext(), "Showing Paired Devices",Toast.LENGTH_SHORT).show();

        final ArrayAdapter adapter = new  ArrayAdapter(this,android.R.layout.simple_list_item_1, plist);

        lview.setAdapter(adapter);
    }

    public void connect(View v){
        if (!BAdapter.isEnabled()) {
            Log.i(TAG, " Bluetooth not enabled yet");
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }
        else {
            if (btnCon.getText().equals("Connect")){
                Log.i(TAG, " In else");
                //Connect button pressed, open DeviceListActivity class, with popup windows that scan for devices
                Intent newIntent = new Intent(MainActivity.this, DeviceListActivity.class);
                startActivityForResult(newIntent, REQUEST_SELECT_DEVICE);
            } else {
                //Disconnect button pressed
                if (mDevice!=null)
                {
                    mService.disconnect();
                }
            }
        }
    }

    public void switchMode(View v) {
       try {
            decider.isAutomated = !decider.isAutomated;
            if(decider.isAutomated){
                btnMode.setText("Automated");
            } else {
                btnMode.setText("Manual");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void switchON(View v) {
        String messageON = "1";
        String msgON = "ON";
        byte[] value;
        try {
            //send data to service
            value = messageON.getBytes("UTF-8");
            mService.writeRXCharacteristic(value);
            //Update the log with time stamp
            String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
            listAdapter.add("["+currentDateTimeString+"] TX: "+ msgON);
            messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
            //edtMessage.setText("");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void switchOFF(View v) {
        String messageOFF = "0";
        String msgOFF = "OFF";
        byte[] value;
        try {
            //send data to service
            value = messageOFF.getBytes("UTF-8");
            mService.writeRXCharacteristic(value);
            //Update the log with time stamp
            String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
            listAdapter.add("["+currentDateTimeString+"] TX: "+ msgOFF);
            messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
           // edtMessage.setText("");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
/*

    public void send(View v) {
            EditText editText = (EditText) findViewById(R.id.sendText);
            String message = editText.getText().toString();
            byte[] value;
            try {
                //send data to service
                value = message.getBytes("UTF-8");
                mService.writeRXCharacteristic(value);
                //Update the log with time stamp
                String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                listAdapter.add("["+currentDateTimeString+"] TX: "+ message);
                messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
                edtMessage.setText("");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
*/

    private final BroadcastReceiver UARTStatusChangeReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            final Intent mIntent = intent;
            gintent = intent;
            //*********************//
            if (action.equals(UartService.ACTION_GATT_CONNECTED)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                        Log.d(TAG, "UART_CONNECT_MSG");
                        btnCon.setText("Disconnect");
                        //edtMessage.setEnabled(true);
                        //btnSend.setEnabled(true);
                        btnOff.setEnabled(true);
                        btnOn.setEnabled(true);
                        btnMode.setEnabled(true);
                        ((TextView) findViewById(R.id.deviceName)).setText(mDevice.getName()+ " - ready");
                        listAdapter.add("["+currentDateTimeString+"] Connected to: "+ mDevice.getName());
                        messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
                        mState = UART_PROFILE_CONNECTED;
                        //showMessage("");
                        Toast.makeText(getApplicationContext(),"Before RSSIIIIII", Toast.LENGTH_SHORT).show();
                        rssi  = mIntent.getShortExtra(mDevice.EXTRA_RSSI,Short.MIN_VALUE);
                        String name = mIntent.getStringExtra(mDevice.EXTRA_NAME);
                        Toast.makeText(getApplicationContext(),"  RSSI: " + name + rssi + "dBm", Toast.LENGTH_SHORT).show();
                        showMessage("rssi of: " + name + rssi);

                        //rssiView.setText(rssi);

                        /* BluetoothLeScanner bluetoothScanner = BAdapter.getBluetoothLeScanner();
                        bluetoothScanner.startScan(new ScanCallback() {
                            @Override
                            public void onScanResult(int callbackType, ScanResult result) {
                                super.onScanResult(callbackType, result);
                                final int rssi = result.getRssi();
                                Log.d(TAG, "rssi is : " + rssi);
                                showMessage("rssi is : " + rssi);
                                //((TextView) findViewById(R.id.rssiValue)).setText(rssi);
                            }
                        });*/
                        /*gatt = mDevice.connectGatt(getApplicationContext(), false, new BluetoothGattCallback() {
                            @Override
                            public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
                                super.onReadRemoteRssi(gatt, rssi, status);
                                Log.d(TAG, "rssi is : " + rssi);
                                showMessage("rssi is : " + rssi);
                                ((TextView) findViewById(R.id.rssiValue)).setText(rssi);
                            }
                        });
                        gatt.readRemoteRssi();*/

                    }
                });
            }

            //*********************//
            if (action.equals(UartService.ACTION_GATT_DISCONNECTED)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                        Log.d(TAG, "UART_DISCONNECT_MSG");
                        showMessage("Disconnect of the device");
                        btnCon.setText("Connect");
                        //edtMessage.setEnabled(false);
                        //btnSend.setEnabled(false);
                        btnOn.setEnabled(false);
                        btnOff.setEnabled(false);
                        btnMode.setEnabled(false);
                        ((TextView) findViewById(R.id.deviceName)).setText("Not Connected");
                        listAdapter.add("["+currentDateTimeString+"] Disconnected to: "+ mDevice.getName());
                        /*rssi  = mIntent.getShortExtra(mDevice.EXTRA_RSSI,Short.MIN_VALUE);
                        String name = mIntent.getStringExtra(mDevice.EXTRA_NAME);
                        showMessage("rssi of: " + name + rssi);*/
                        //rssiView.setText(rssi);
                      /*  BluetoothLeScanner bluetoothScanner = BAdapter.getBluetoothLeScanner();
                        bluetoothScanner.startScan(new ScanCallback() {
                            @Override
                            public void onScanResult(int callbackType, ScanResult result) {
                                super.onScanResult(callbackType, result);
                                final int rssi = result.getRssi();
                                Log.d(TAG, "rssi is : " + rssi);
                                showMessage("rssi is : " + rssi);
                               // ((TextView) findViewById(R.id.rssiValue)).setText(rssi);
                                rssiView.setText(rssi);
                            }
                        });
                      */
                      /*gatt = mDevice.connectGatt(getApplicationContext(), false, new BluetoothGattCallback() {
                            @Override
                            public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
                                super.onReadRemoteRssi(gatt, rssi, status);
                                Log.d(TAG, "rssi is : " + rssi);
                                showMessage("rssi is : " + rssi);
                                ((TextView) findViewById(R.id.rssiValue)).setText(rssi);
                            }
                        });
                        gatt.readRemoteRssi();*/
                      mState = UART_PROFILE_DISCONNECTED;
                        mService.close();
                        //setUiState();

                    }
                });
            }


            //*********************//
            if (action.equals(UartService.ACTION_GATT_SERVICES_DISCOVERED)) {
                mService.enableTXNotification();
            }
            //*********************//
            if (action.equals(UartService.ACTION_DATA_AVAILABLE)) {

                final byte[] txValue = intent.getByteArrayExtra(UartService.EXTRA_DATA);
                runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            String text = new String(txValue, "UTF-8");
                            String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                            listAdapter.add("["+currentDateTimeString+"] RX: "+text);
                            messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);

                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }
                    }
                });
            }
            //*********************//
            if (action.equals(UartService.DEVICE_DOES_NOT_SUPPORT_UART)){
                showMessage("Device doesn't support UART. Disconnecting");
                mService.disconnect();
            }

//            if(BluetoothDevice.ACTION_FOUND.equals(action)) {
//                int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,Short.MIN_VALUE);
//                String name = intent.getStringExtra(BluetoothDevice.EXTRA_NAME);
//                TextView rssi_msg = (TextView) findViewById(R.id.textView1);
//                rssi_msg.setText(rssi_msg.getText() + name + " => " + rssi + "dBm\n");
//            }


        }
    };

    private void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

    }

    private void service_init() {
        Intent bindIntent = new Intent(this, UartService.class);
        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

        LocalBroadcastManager.getInstance(this).registerReceiver(UARTStatusChangeReceiver, makeGattUpdateIntentFilter());
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UartService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(UartService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(UartService.DEVICE_DOES_NOT_SUPPORT_UART);
        return intentFilter;
    }

    //UART service connected/disconnected
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            mService = ((UartService.LocalBinder) rawBinder).getService();
            Log.d(TAG, "onServiceConnected mService= " + mService);
            if (!mService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }

        }

        public void onServiceDisconnected(ComponentName classname) {
            ////     mService.disconnect(mDevice);
            mService = null;
        }
    };

    private Handler mHandler = new Handler() {
        @Override

        //Handler events that received from UART service
        public void handleMessage(Message msg) {

        }
    };

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");

        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(UARTStatusChangeReceiver);
        } catch (Exception ignore) {
            Log.e(TAG, ignore.toString());
        }
        unbindService(mServiceConnection);
        mService.stopSelf();
        mService= null;

    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
        if(isSensorPresent)
        {
            mSensorManager.unregisterListener(this);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if(isSensorPresent)
        {
            mSensorManager.registerListener(this, mStepSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
        if (!BAdapter.isEnabled()) {
            Log.i(TAG, "onResume - BT not enabled yet");
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case REQUEST_SELECT_DEVICE:
                //When the DeviceListActivity return, with the selected device address
                if (resultCode == Activity.RESULT_OK && data != null) {
                    String deviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
                    mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(deviceAddress);

                    Log.d(TAG, "... onActivityResultdevice.address==" + mDevice + "mserviceValue" + mService);
                    ((TextView) findViewById(R.id.deviceName)).setText(mDevice.getName()+ " - connecting");
                    mService.connect(deviceAddress);


                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(this, "Bluetooth has turned on ", Toast.LENGTH_SHORT).show();

                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, "Problem in BT Turning ON ", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            default:
                Log.e(TAG, "wrong request code");
                break;
        }
    }

    /*@Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

    }*/



    @Override
    public void onBackPressed() {
        if (mState == UART_PROFILE_CONNECTED) {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
            showMessage("Home Automation is running in background.\n             Disconnect to exit");
        }
        else {
           new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.popup_title)
                    .setMessage(R.string.popup_message)
                    .setPositiveButton(R.string.popup_yes, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.popup_no, null)
                    .show();
        }
    }
}

