package com.example.bluetoothtest;

public interface ISensorMeasurement {
    public void setLight(float newValue);
    public void setProximity(float newValue);
    public void setStep(int newValue);
    public void setRSSI(float newValue);
    public float getLight();
    public float getProximity();
    public float[] getAccelerometer();
    public int getStep();
    public float getRSSI();
}
