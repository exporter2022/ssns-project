package com.example.bluetoothtest.data;

import android.widget.Toast;

import com.example.bluetoothtest.data.model.LoggedInUser;

import java.io.IOException;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    public Result<LoggedInUser> login(String username, String password) {

        try {
            String UserName = username;
            String Pwd = password;
            Exception e1 = null;
            if (UserName.equalsIgnoreCase("Admin@gmail.com") && Pwd.equals("Admin@123")) {
                // Intent MainIntent = new Intent(LoginActivity.this, MainActivity.class);
                //startActivity(MainIntent);
                // Toast.makeText(LoginActivity.this,"You are Sign in Successfuly.", Toast.LENGTH_LONG).show();
                LoggedInUser fakeUser =
                        new LoggedInUser(
                                java.util.UUID.randomUUID().toString(),
                                username);
                return new Result.Success<>(fakeUser);
            }
            else
            {
                return new Result.Error(new IOException("Error logging in", e1));
                //Toast.makeText(getApplicationContext(),"Sorry,User Name or Password is incorrect.", Toast.LENGTH_LONG).show();
            }
            // TODO: handle loggedInUser authentication

        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}