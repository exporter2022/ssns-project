package com.example.bluetoothtest;

import android.bluetooth.BluetoothDevice;

public class LightController implements ILightController{
    private UartService service;
    private BluetoothDevice device;
    public LightController(UartService mService, BluetoothDevice mDevice) {
        this.service = mService;
        this.device = mDevice;
    }
    public void turnOn() {
        this.send("1");
    }

    public void turnOff() {
        this.send("0");
    }

    private void send(String message) {
        byte[] value = message.getBytes();
        if(this.device!=null) {
            this.service.writeRXCharacteristic(value);
        }
    }
}
